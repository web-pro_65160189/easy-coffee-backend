import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const userRespository = AppDataSource.getRepository(User);
    const rolesRepository = AppDataSource.getRepository(Role);

    const adminRole = await rolesRepository.findOneBy({ id: 1 });
    const userRole = await rolesRepository.findOneBy({ id: 2 });

    await userRespository.clear();
    console.log("Inserting a new user into the memory...");
    var user = new User();
    user.id = 1;
    user.email = "test@gmail.com";
    user.password = "1";
    user.gender = "male";
    user.roles = [];
    user.roles.push(adminRole);
    user.roles.push(userRole);

    console.log("Inserting a new user into the database...");
    await userRespository.save(user);

    console.log("Inserting a new user into the memory...");
    user = new User();
    user.id = 2;
    user.email = "test01@gmail.com";
    user.password = "1234";
    user.gender = "male";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the database...");
    await userRespository.save(user);

    console.log("Inserting a new user into the memory...");
    user = new User();
    user.id = 3;
    user.email = "test02@gmail.com";
    user.password = "1234";
    user.gender = "female";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the database...");
    await userRespository.save(user);

    const users = await userRespository.find({ relations: { roles: true } });
    console.log(JSON.stringify(users, null, 2));

    const roles = await rolesRepository.find({ relations: { users: true } });
    console.log(JSON.stringify(roles, null, 2));
  })
  .catch((error) => console.log(error));
